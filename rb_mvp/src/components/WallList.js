import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import WallListCat from './WallList/WallListCat';
import WallListHome from './05-page/WallListHome';

class WallList extends Component {
    render() {
        return (
            <Switch>
               <Route path="/wall_list/cat" component={WallListCat} />  

               <Route path="/wall_list" component={WallListHome} />  
            </Switch>
        );
    }
};

export default WallList;