import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import WallList from './components/WallList';
import Home from './components/Home';

const App = () => {
    
    return (
        <div>
            <BrowserRouter>
                {/* <TopBar /> */}
                <Switch>
                {/* <Route path="/wordwall" component={WordWall} /> ? */}
                <Route path="/wall_list" component={WallList} /> 
                
                <Route path="/"  component={Home} />
                </Switch>
            </BrowserRouter>
        </div>
    );
    
};

export default App;