import React, { Component } from 'react';

import WordCount from '../01_atom/WordCount';
import WallDetails from '../02-molecules/WallDetail';
import WordWall from '../02-molecules/WordWall';

class WallID extends Component {
    render() {
        return (
        <section className="section-projects">
            <br/>
            <WordCount class="page" />
            <br/>
            <div className="Layout_WordWall">
                <WallDetails />
                <WordWall />

            </div>
            <div className="flex-center">
                <button className="PlayLearn">Play & Learn</button>
            </div>
        </section>

        );
    }
};

export default WallID;