import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectWall } from '../../actions';



class WallList extends Component {
    renderList() {
        return this.props.walls.map((wall) => {
            return (
                <div key={wall.title} >
                    <div>
                        <button
                        onClick={() => this.props.selectWall(wall)}
                        >Select</button>
                    </div>
                    <div>{wall.title} </div>
                </div>
            )
        })
    }

    render() {
        // console.log(this.props);
        return ( 
            <div>
                {/* <h1>WallList organisms</h1> */}
                {this.renderList()}

            </div>
        );
    }
};
const mapStateToProps = (state) => {
    // console.log(state);
    
    return { walls: state.walls };
}

export default connect(mapStateToProps, { selectWall })(WallList);