import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class SideNavItem extends Component {
    render() {
        return (
            <li className="nav-item">
                <NavLink className="nav-link" exact activeClassName="active-navlink" to={`${this.props.link}`}>{this.props.page}</NavLink>                
            </li>
        );
    }
};

export default SideNavItem;