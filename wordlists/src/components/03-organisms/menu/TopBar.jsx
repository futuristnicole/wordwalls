import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ToggleModalTopNav from './TopNav/ToggleModalTopNav';
import TopNav from './TopNav/TopNav';

// import NavItem from './Nav-item';

class TopBar extends Component {
    render() {
        return (
            <header className="topbar">
                <Link to="/" className="logo"><p className="logotext"><span className="logotext-Read">Read</span>Boot</p></Link>
                <nav className="topnav">
                    <ul className="nav-top">
                        <TopNav />
                    </ul>
                    <ToggleModalTopNav/>
                </nav>
                {/* <h1>TopBar</h1> */}
            </header>
        );
    }
};

export default TopBar;