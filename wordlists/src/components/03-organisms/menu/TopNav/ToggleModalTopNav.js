import React from 'react';
import useToggle from '../../../00-base/Hooks/useToggle';
import ModalTopNav from './ModalTopNav';


function ToggleModalTopNav() {
    const [isTopNav, toggleIsTopNav] = useToggle(false);
    
    return (
        <div>
            <h1 className="hambuger__h1" onClick={toggleIsTopNav}>{isTopNav ?  <><div className="hambuger"></div><ModalTopNav /> < /> : <div className="hambuger"></div>}</h1>
            
        </div>
    )
}

export default ToggleModalTopNav;