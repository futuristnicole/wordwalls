import React from 'react'
import ReactDOM from 'react-dom';
// import NavItem from '../Nav-item';
import TopNav from './TopNav';


const ModalTopNav = props => {
    return ReactDOM.createPortal(
        <div className="modalTopNav">
            <ul className="nav-top_modal">
                <TopNav />
            </ul>
        </div>,
        document.querySelector('#modalTopNav')
    )
};

export default ModalTopNav;