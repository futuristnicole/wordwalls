import React, { Component } from 'react'
// import ReactDOM from 'react-dom';
import NavItem from '../Nav-item';

class TopNav extends Component {
    render() {
        return (
            <>
                <NavItem  link="/" page='Home' />
                <NavItem  link='/walls' page='Word Walls' />
                
                <NavItem  link='/about' page='About' />
                <NavItem  link='/contact' page='Contact' />  
            < />
        )
    }
};

export default TopNav;
