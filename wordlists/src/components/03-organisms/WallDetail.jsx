import React from 'react';
import { connect } from 'react-redux';

const WallDetail = ({ wall }) => {
    console.log(wall);
    if ( !wall ) {
        return <div>select a Word Wall</div>;
    }
    return  (
        <h3>{wall.title}</h3>
    );
};

const mapStateToProps = (state) => {
    // console.log(state);
    
    return { wall: state.selectedWall };
}

export default connect(mapStateToProps)(WallDetail);