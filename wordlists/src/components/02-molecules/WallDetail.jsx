import React from 'react';
import { connect } from 'react-redux';

const WallDetail = ({ wall }) => {
    if (!wall) {
        return (
            <div> <h1>No wall Selected.</h1>
            </div>
        )
    }
    return (
        <div className="">
            <h1 className="WordWallName">{wall.title}</h1>
            <h3 className="h3-WordWall">{wall.headline}</h3>
            <p>{wall.description}</p>
            <p className="bold">{wall.subject} - {wall.topic}</p>
            <div className="Person-box">
               <img className="img-contain__Video" 
                    src={`../img/WallCard/${wall.img}.jpg`} 
                    alt={`${wall.alt}`} >
                </img> 
            </div>
        </div>
        
    );
};

const mapStateToProps = (state) => {
    console.log(state);
    
    return { wall: state.selectedWall };
}

export default connect(mapStateToProps)(WallDetail);