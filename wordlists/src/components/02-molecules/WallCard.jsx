import React, { Component } from 'react';
import { connect } from 'react-redux';

import WordCount from '../01_atom/WordCount';
import { selectWall } from '../../actions/SelectWall';
import { Link } from 'react-router-dom';


class WallCard extends Component {
    renderList() {
        return this.props.walls.map((wall) => {
            return ( 
                // <div className="grid-WallCard" key={wall.title} >
                    <Link className="grid-WallCard" 
                        onClick={() => this.props.selectWall(wall)}
                        to={`/wall/${wall.title}`} >             
                        <img className="img-contain__WallCard"  
                            src={`../img/WallCard/${wall.img}.jpg`} 
                            alt={`${wall.alt}`}/>
                        <div>
                            <h3 className="WallCard_text">{wall.title}</h3>
                            <p>{wall.headline}</p>
                        </div>
                        <WordCount class="card" />
                    </Link>                
                // </div>
            );
        } );
    }
    render() {
        console.log(this.props);
        return ( 
            <div>
                {/* <h1>WallList organisms</h1> */}
                <div className="grid-3a">
                    {this.renderList()}
                </div>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    console.log(state);
    
    return { walls: state.walls };
}

export default connect(mapStateToProps, { selectWall })(WallCard);