import { combineReducers } from 'redux';

const wallsReducer = () => {
    return [
        { title: 'Ending in "at"',
        headline: 'Words ending in "at"',
        description: 'Help the learn to learn beginning sounds in words. Words ending in "at"',
        topic: 'ending sounds',
        subject: 'Spelling',
        grade: '0',
        img: 'Yash',
        alt: ''
        },
        { title: 'Ending in "an"',
        headline: 'Words ending in "an"',
        description: 'Help the learn to learn beginning sounds in words. Words ending in "an"',
        topic: 'ending sounds',
        subject: 'Spelling',
        grade: '0',
        img: 'Disney1',
        alt: ''
        },
        { title: 'Ending in "in"',
        headline: 'Words ending in "in"',
        description: 'Help the learn to learn beginning sounds in words. Words ending in "in"',
        topic: 'ending sounds',
        subject: 'Spelling',
        grade: '0',
        img: 'Yash',
        alt: ''
        },
        { title: 'Ending in "en"',
        headline: 'Words ending in "at"',
        description: 'Help the learn to learn beginning sounds in words. Words ending in "in"',
        topic: 'ending sounds',
        subject: 'Spelling',
        grade: '0',
        img: 'Disney1',
        alt: ''
        },
    ];
};

const selectedWallReducer = (selectedWall=null, action) => {
    if (action.type === 'WALL_SELECTED') {
        return action.payload;
    }
    return selectedWall;
}

export default combineReducers({
    walls: wallsReducer,
    selectedWall: selectedWallReducer
})