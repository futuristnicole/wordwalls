import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import TopBar from './components/03-organisms/menu/TopBar';
import wallID from './components/05_pages/WallID';
import Walls from './components/05_pages/ListID';
// import WordLists from './components/06_route/WordWalls';
import Home from './components/05_pages/Home';


const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <TopBar />
        <Switch>
          <Route path="/wall/:wallID" component={wallID} /> 
          <Route path="/:categoryID" component={Walls} /> 
          {/* <Route path="/walls" component={Walls} />  */}
          {/* <Route path="/walls" component={WordLists} />  */}
         
          <Route path="/"  component={Home} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
